package fr.persistance

import com.typesafe.config.ConfigException.Null
import org.openapitools.server.model.{Categorie, CategorieComplete, Tache, User, UserComplete}

import java.sql.{Connection, DriverManager, PreparedStatement, Statement}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.openapitools.server.model.TacheComplete
import java.sql.ResultSet
import org.openapitools.server.model.TacheUpdate
import java.util.Locale.Category

object Database {

  def getConnection: Connection = {
    val url = "jdbc:sqlite:taskmanager.db"
    DriverManager.getConnection(url)
  }

  def createTables(): Unit = {
    val connection = getConnection
    val stmt = connection.createStatement()

    val sqlUsers =
      """
    CREATE TABLE IF NOT EXISTS users (
        idUser INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        postalCode INTEGER,
        city TEXT
    );
"""

    val sqlCategories ="""CREATE TABLE IF NOT EXISTS categories (idCategory INTEGER PRIMARY KEY AUTOINCREMENT,
     name TEXT,idUser INTEGER, FOREIGN KEY (idUser) REFERENCES users (idUser));"""

    val sqlTasks = """ CREATE TABLE IF NOT EXISTS tasks ( idTask INTEGER PRIMARY KEY AUTOINCREMENT, idCategory INTEGER,label TEXT,status TEXT, FOREIGN KEY (idCategory) REFERENCES categories (idCategory));"""

    stmt.execute(sqlUsers)
    stmt.execute(sqlCategories)
    stmt.execute(sqlTasks)

    stmt.close()
    connection.close()
  }


  def createUser(user : User): Future[Long] = Future{
    val url = "jdbc:sqlite:taskmanager.db"
    var connection: Connection = null
    try {
      connection = DriverManager.getConnection(url)
      val sql = "INSERT INTO users (username, postalCode, city) VALUES (?, ?, ?)"
      val preparedStatement: PreparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)


      preparedStatement.setString(1, user.username.orNull)
      preparedStatement.setString(2, user.postalCode.orNull)
      preparedStatement.setString(3, user.city.orNull)

      preparedStatement.executeUpdate()
      val rs = preparedStatement.getGeneratedKeys
      if (rs.next()) rs.getLong(1) else -1L
    } catch {
      case e: Exception => e.printStackTrace(); -1L
    } finally {
      if (connection != null) connection.close()
    }
  }


  def createCategory(category : Categorie): Future[Long]= {
    val connection: Connection = DriverManager.getConnection("jdbc:sqlite:taskmanager.db")
    val sql = "INSERT INTO categories (name, idUser) VALUES (?, ?)"
    val preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)

    preparedStatement.setString(1, category.name.orNull)
    preparedStatement.setLong(2, category.idUser.getOrElse(0))

    preparedStatement.executeUpdate()
    val rs = preparedStatement.getGeneratedKeys
    val generatedId = if (rs.next()) rs.getLong(1) else -1L

    rs.close()
    preparedStatement.close()
    connection.close()
    Future{generatedId}
  }


  def createTask(task :Tache): Future[Long] = Future{
    val connection: Connection = DriverManager.getConnection("jdbc:sqlite:taskmanager.db")
    val sql = "INSERT INTO tasks (idCategory, label, status) VALUES (?, ?, ?)"
    val preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)

    preparedStatement.setLong(1, task.idCategory.getOrElse(-1))
    preparedStatement.setString(2, task.label.getOrElse( "null"))
    preparedStatement.setString(3, "init")

    preparedStatement.executeUpdate()
    val rs = preparedStatement.getGeneratedKeys
    val generatedId = if (rs.next()) rs.getLong(1) else -1L

    rs.close()
    preparedStatement.close()
    connection.close()
    generatedId
  }

  def updateTask(updatedTask: TacheUpdate): Future[Unit] = Future {
    val connection: Connection = DriverManager.getConnection("jdbc:sqlite:taskmanager.db")
    val sql = "UPDATE tasks SET status = ? WHERE idTask = ?"
    val preparedStatement = connection.prepareStatement(sql)

    preparedStatement.setString(1, updatedTask.status.getOrElse("init"))
    preparedStatement.setLong(2, updatedTask.idTask.getOrElse(-1))

    preparedStatement.executeUpdate()

    preparedStatement.close()
    connection.close()
  }

  def getTask(id: Long): Future[Option[TacheComplete]] = Future {
    val connection: Connection = DriverManager.getConnection("jdbc:sqlite:taskmanager.db")
    val sql = "SELECT idTask, idCategory, label, status FROM tasks WHERE idTask = ?"
    val preparedStatement = connection.prepareStatement(sql)

    preparedStatement.setLong(1, id)
    val resultSet: ResultSet = preparedStatement.executeQuery()

    val result = if (resultSet.next()) {
      Some(
        TacheComplete(
          Some(resultSet.getLong("idTask")),
          Some(resultSet.getLong("idCategory")),
          Some(resultSet.getString("label")),
          Some(resultSet.getString("status"))
        )
      )
    } else None

    resultSet.close()
    preparedStatement.close()
    connection.close()

    result
  }

  def getCategory(id: Long): Future[Option[CategorieComplete]] = Future {
    val connection: Connection = DriverManager.getConnection("jdbc:sqlite:taskmanager.db")
    val sql = "SELECT c.idCategory, c.name, u.idUser  " +
              "FROM categories c LEFT JOIN users u ON c.idUser = u.idUser " +
              "WHERE c.idCategory = ?"
    val preparedStatement = connection.prepareStatement(sql)

    preparedStatement.setLong(1, id)
    val resultSet: ResultSet = preparedStatement.executeQuery()

    val result = if (resultSet.next()) {
      Some(
        CategorieComplete(
          Some(resultSet.getLong("idCategory")),
          Some(resultSet.getString("name")),
          Some(resultSet.getInt("idUser")),
        )
      )
    } else None

    resultSet.close()
    preparedStatement.close()
    connection.close()

    result
  }

  def getCategories: Future[Seq[CategorieComplete]] = {
    val connection: Connection = getConnection
    val sql = """
      SELECT c.idCategory, c.name, u.idUser, u.username, u.postalCode, u.city
      FROM categories c
      LEFT JOIN users u ON c.idUser = u.idUser
  """
    val statement = connection.createStatement()
    val resultSet = statement.executeQuery(sql)

    var categories = Seq.empty[CategorieComplete]
    while (resultSet.next()) {
      val idCategory = resultSet.getLong("idCategory")
      val name = resultSet.getString("name")
      val idUser = resultSet.getInt("idUser")
      

      categories = categories :+ CategorieComplete(Some(idCategory), Option(name),Some(idUser))
    }

    resultSet.close()
    statement.close()
    connection.close()
    Future{categories}
  }

   def getUsers: Future[Seq[UserComplete]] = {
    val connection: Connection = getConnection
    val sql = """
      SELECT u.idUser, u.username, u.postalCode, u.city
      FROM users u
    """
    val statement = connection.createStatement()
    val resultSet = statement.executeQuery(sql)

    var users = Seq.empty[UserComplete]
    while (resultSet.next()) {
      val idUser = resultSet.getLong("idUser")
      val username = resultSet.getString("username")
      val postalCode = resultSet.getString("postalCode")
      val city = resultSet.getString("city")

      val user = UserComplete(
        Some(idUser),
        Option(username),
        Option(postalCode),
        Option(city)
      )

      users = users :+ user
    }

    resultSet.close()
    statement.close()
    connection.close()
    Future(users)
  }

   def geTasks: Future[Seq[TacheComplete]] = {
    val connection: Connection = getConnection
    val sql = """
      SELECT t.idTask, t.idCategory, t.label, t.status
      FROM tasks t
    """
    val statement = connection.createStatement()
    val resultSet = statement.executeQuery(sql)

    var tasks = Seq.empty[TacheComplete]
    while (resultSet.next()) {
      val idTask = resultSet.getLong("idTask")
      val idCategory = resultSet.getLong("idCategory")
      val label = resultSet.getString("label")
      val status = resultSet.getString("status")

      val task = TacheComplete(
        Some(idTask),
        Some(idCategory),
        Some(label),
        Some(status)
      )

      tasks = tasks :+ task
    }

    resultSet.close()
    statement.close()
    connection.close()
    Future(tasks)
  }







}
