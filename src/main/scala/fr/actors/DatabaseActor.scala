package fr.actors

import fr.persistance.Database
import org.apache.pekko.actor.typed.{ActorRef, Behavior}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.openapitools.server.model.User

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object DatabaseActor {
  sealed trait Command
  case class CreateUser(user: User, replyTo: ActorRef[Response]) extends Command
  case class UpdateUserCity(user: User, cityName: String, replyTo: ActorRef[Response]) extends Command
  private case class WrappedResponse(response: Response, replyTo: ActorRef[Response]) extends Command

  sealed trait Response
  case class UserCreated(userId: Long) extends Response
  case class UserCreationFailed(error: String) extends Response

  def apply(requesterActor: ActorRef[RequesterActor.Command]): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.receiveMessage {
      case CreateUser(user, replyTo) =>
        requesterActor ! RequesterActor.FetchCity(user.postalCode.getOrElse("0000"), context.self, user, replyTo)
        Behaviors.same

      case UpdateUserCity(user, cityName, originalReplyTo) =>
        val updatedUser = user.copy(city = Some(cityName))
        Database.createUser(updatedUser).onComplete {
          case Success(userId) => originalReplyTo ! UserCreated(userId)
          case Failure(exception) => originalReplyTo ! UserCreationFailed(exception.getMessage)
        }
        Behaviors.same

      case WrappedResponse(UserCreated(userId), replyTo) =>
        replyTo ! UserCreated(userId)
        Behaviors.same
      case WrappedResponse(UserCreationFailed(error), replyTo) =>
        replyTo ! UserCreationFailed(error)
        Behaviors.same
    }
  }
}
