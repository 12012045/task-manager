package fr.actors

import org.apache.pekko.actor.typed.{ActorRef, ActorSystem}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.http.scaladsl.Http
import org.openapitools.server.model.User
import fr.persistance.Database

object Main {
  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "taskManagerSystem")

    implicit val httpExt = Http()(system.classicSystem)
    
    val requesterActor = system.systemActorOf(RequesterActor(httpExt), "requesterActor")

    val databaseActor = system.systemActorOf(DatabaseActor(requesterActor), "databaseActor")
    Database.createTables()
    
    val dummyActor: ActorRef[DatabaseActor.Response] =
      system.systemActorOf(Behaviors.ignore[DatabaseActor.Response], "dummyActor")

    val exampleUser = User(Some("Tetereou"), Some("93800"),None)

    databaseActor ! DatabaseActor.CreateUser(exampleUser, replyTo = dummyActor)
    println("\u001b[32mOperation terminated. Please start the HTTP server and make a curl to get all users and see the updates.\u001b[0m")

  }
}