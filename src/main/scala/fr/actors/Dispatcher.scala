

package fr.actors

import fr.persistance.Database
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem, Behavior}
import org.apache.pekko.http.scaladsl.model.headers.CacheDirectives.public
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.CategorieApi
import org.openapitools.server.api.impl.{CategoryApiServiceImpl, CategoryMarshaller}
import org.openapitools.server.api.impl.UserApiServiceImpl
import org.openapitools.server.api.UserApi
import org.openapitools.server.api.impl.UserMarshaller
import org.openapitools.server.api.TacheApi
import org.openapitools.server.api.impl.TacheApiServiceImpl
import org.openapitools.server.api.impl.TacheMarshaller

import scala.concurrent.ExecutionContext.Implicits.global





class Dispatcher(databaseActor: ActorRef[DatabaseActor.Command])(implicit val system: ActorSystem[_]) {

  private val userApiService = new UserApiServiceImpl(databaseActor)
  private val userApi = new UserApi(userApiService, UserMarshaller)


  private val categoryApi = new CategorieApi(CategoryApiServiceImpl,CategoryMarshaller)
  private val tacheApi = new TacheApi(TacheApiServiceImpl,TacheMarshaller)

  def routes: Route = {
   
      concat(
             categoryApi.route,
             userApi.route,
             tacheApi.route
          )
        }


  
}
