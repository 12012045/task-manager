package fr.actors
import org.apache.pekko
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem, Behavior}
import org.apache.pekko.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import org.apache.pekko.http.scaladsl.{Http, HttpExt}
import pekko.http.scaladsl.model._
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import spray.json.DefaultJsonProtocol.{StringJsonFormat, immSeqFormat, jsonFormat1}
import spray.json.{JsValue, RootJsonFormat}
import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.openapitools.server.model.User
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

// Place and Zippopotam case classes with custom JSON formats
case class Place(placeName: String)

object Place {
  implicit object PlaceJsonFormat extends RootJsonFormat[Place] {
    def write(p: Place): JsValue = JsObject("place name" -> JsString(p.placeName))
    def read(json: JsValue): Place = {
      val fields = json.asJsObject.fields
      Place(placeName = fields("place name").convertTo[String])
    }
  }
}

case class Zippopotam(places: Seq[Place])

object Zippopotam {
  implicit object ZippopotamJsonFormat extends RootJsonFormat[Zippopotam] {
    def write(z: Zippopotam): JsValue = JsObject("places" -> JsArray(z.places.map(_.toJson).toVector))
    def read(json: JsValue): Zippopotam = {
      val fields = json.asJsObject.fields
      Zippopotam(places = fields("places").convertTo[Seq[Place]])
    }
  }
}

object RequesterActor {
  sealed trait Command
  case class FetchCity(postalCode: String, replyTo: ActorRef[DatabaseActor.Command], user: User, originalReplyTo: ActorRef[DatabaseActor.Response]) extends Command

  def apply(http: HttpExt)(implicit system: ActorSystem[_]): Behavior[Command] = Behaviors.setup { context =>
    import spray.json.DefaultJsonProtocol._
    Behaviors.receiveMessage {
      case FetchCity(postalCode, replyToDatabaseActor, user, originalReplyTo) =>
        val responseFuture = http.singleRequest(HttpRequest(uri = s"https://api.zippopotam.us/fr/$postalCode"))
        responseFuture.onComplete {
          case Success(res) if res.status == StatusCodes.OK =>
            Unmarshal(res.entity).to[Zippopotam].onComplete {
              case Success(zippopotam) =>
                val cityName = zippopotam.places.headOption.map(_.placeName).getOrElse("Unknown City")
                replyToDatabaseActor ! DatabaseActor.UpdateUserCity(user, cityName, originalReplyTo)
              case Failure(_) =>
                replyToDatabaseActor ! DatabaseActor.UpdateUserCity(user, "Unknown City", originalReplyTo)
            }
          case _ =>
            replyToDatabaseActor ! DatabaseActor.UpdateUserCity(user, "Unknown City", originalReplyTo)
        }
        Behaviors.same
    }
  }
}
