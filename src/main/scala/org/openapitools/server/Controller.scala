package org.openapitools.server

import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.CategorieApi
import org.openapitools.server.api.TacheApi
import org.openapitools.server.api.UserApi

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.ActorMaterializer

class Controller(categorie: CategorieApi, tache: TacheApi, user: UserApi)(implicit system: ActorSystem, materializer: ActorMaterializer) {

    lazy val routes: Route = categorie.route ~ tache.route ~ user.route 

    Http().bindAndHandle(routes, "0.0.0.0", 9000)
}