package org.openapitools.server.model


/**
 * @param idTask  for example: ''10250''
 * @param idCategory  for example: ''1007''
 * @param label  for example: ''todo1''
 * @param status Statut de la tache for example: ''encours''
*/
final case class TacheComplete (
  idTask: Option[Long] = None,
  idCategory: Option[Long] = None,
  label: Option[String] = None,
  status: Option[String] = None
)

