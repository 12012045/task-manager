package org.openapitools.server.model


/**
 * @param idCategory  for example: ''1007''
 * @param label  for example: ''todo1''
*/
final case class Tache (
  idCategory: Option[Long] = None,
  label: Option[String] = None
)

