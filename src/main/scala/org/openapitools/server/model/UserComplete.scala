package org.openapitools.server.model


/**
 * @param idUser  for example: ''10250''
 * @param username  for example: ''theUser''
 * @param postalCode Code Postal de l'utilisateur for example: ''95370''
 * @param city Ville de l'utilsateur for example: ''Saint-Denis''
*/
final case class UserComplete (
  idUser: Option[Long] = None,
  username: Option[String] = None,
  postalCode: Option[String] = None,
  city: Option[String] = None
)

