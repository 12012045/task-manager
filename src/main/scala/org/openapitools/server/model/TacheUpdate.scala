package org.openapitools.server.model


/**
 * @param idTask  for example: ''178''
 * @param status Statut de la tache for example: ''encours''
*/
final case class TacheUpdate (
  idTask: Option[Long] = None,
  status: Option[String] = None
)

