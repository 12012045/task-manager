package org.openapitools.server.model

import java.time.OffsetDateTime

/**
 * @param name  for example: ''travail''
 * @param idUser  for example: ''19872''
 * @param date  for example: ''null''
*/
final case class Categorie (
  name: Option[String] = None,
  idUser: Option[Long] = None,
  //date: Option[OffsetDateTime] = None
)

