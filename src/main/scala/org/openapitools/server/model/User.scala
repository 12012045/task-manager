package org.openapitools.server.model


/**
 * @param username  for example: ''theUser''
 * @param postalCode Code Postal de l'utilisateur for example: ''93200''
 * @param city Ville de l'utilsateur for example: ''Saint-Denis''
*/
final case class User (
  username: Option[String] = None,
  postalCode: Option[String] = None,
  city: Option[String] = None
)

