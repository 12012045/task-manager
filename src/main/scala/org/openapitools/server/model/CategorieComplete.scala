package org.openapitools.server.model

import java.time.OffsetDateTime

/**
 * @param idCategory  for example: ''12885''
 * @param name  for example: ''travail''
 * @param idUser  for example: ''85''
 * @param date  for example: ''null''
*/
final case class CategorieComplete (
  idCategory: Option[Long] = None,
  name: Option[String] = None,
  idUser: Option[Int] = None,
  //date: Option[OffsetDateTime] = None
)

