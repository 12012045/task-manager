package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.Tache
import org.openapitools.server.model.TacheComplete
import org.openapitools.server.model.TacheUpdate


class TacheApi(
    tacheService: TacheApiService,
    tacheMarshaller: TacheApiMarshaller
) {

  
  import tacheMarshaller._

  lazy val route: Route =
    path("tache") { 
      post {  
            entity(as[Tache]){ tache =>
              tacheService.createTask(tache = tache)
            }
      }
    } ~
    path("tache" / IntNumber) { (idTache) => 
      get {  
            tacheService.getTask(idTache = idTache)
      }
    } ~
    path("tache" ) {   
      get {  
            tacheService.getTasks()
      }
    } ~
    path("tache") { 
      put {  
            entity(as[TacheUpdate]){ tacheUpdate =>
              tacheService.updateTask(tacheUpdate = tacheUpdate)
            }
      }
    }
}


trait TacheApiService {

  def createTask201: Route =
    complete((201, "opération effectuée avec succès"))
  /**
   * Code: 201, Message: opération effectuée avec succès
   */
  def createTask(tache: Tache): Route

  def getTask200(responseTacheComplete: TacheComplete)(implicit toEntityMarshallerTacheComplete: ToEntityMarshaller[TacheComplete]): Route =
    complete((200, responseTacheComplete))
  def getTask404: Route =
    complete((404, "aucune categorie associe"))
  /**
   * Code: 200, Message: successful operation, DataType: TacheComplete
   * Code: 404, Message: aucune categorie associe
   */
  def getTask(idTache: Int)
      (implicit toEntityMarshallerTacheComplete: ToEntityMarshaller[TacheComplete]): Route
  
  def getTasks()
      (implicit toEntityMarshallerTacheCompletearray: ToEntityMarshaller[Seq[TacheComplete]]): Route

  def updateTask200: Route =
    complete((200, "opération effectuée avec succès"))
  /**
   * Code: 200, Message: opération effectuée avec succès
   */
  def updateTask(tacheUpdate: TacheUpdate): Route

}

trait TacheApiMarshaller {
  implicit def fromEntityUnmarshallerTacheUpdate: FromEntityUnmarshaller[TacheUpdate]

  implicit def fromEntityUnmarshallerTache: FromEntityUnmarshaller[Tache]



  implicit def toEntityMarshallerTacheComplete: ToEntityMarshaller[TacheComplete]

  implicit def toEntityMarshallerTacheCompletearray: ToEntityMarshaller[Seq[TacheComplete]]

}

