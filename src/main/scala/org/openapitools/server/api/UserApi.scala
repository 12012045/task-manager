package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.User
import org.openapitools.server.model.UserComplete


class UserApi(
    userService: UserApiService,
    userMarshaller: UserApiMarshaller
) {

  
  import userMarshaller._

  lazy val route: Route =
    path("user") { 
      post {  
            entity(as[User]){ user =>
              userService.createUser(user = user)
            }
      }
    } ~
    path("user") { 
      get {  
            userService.getUsers()
      }
    }
}


trait UserApiService {

  def createUser200: Route =
    complete((200, "opération effectuée avec succès"))
  /**
   * Code: 200, Message: opération effectuée avec succès
   */
  def createUser(user: User): Route

  def getUsers200(responseUserCompletearray: Seq[UserComplete])(implicit toEntityMarshallerUserCompletearray: ToEntityMarshaller[Seq[UserComplete]]): Route =
    complete((200, responseUserCompletearray))
  /**
   * Code: 200, Message: opération effectuée avec succès, DataType: Seq[UserComplete]
   */
  def getUsers()
      (implicit toEntityMarshallerUserCompletearray: ToEntityMarshaller[Seq[UserComplete]]): Route

}

trait UserApiMarshaller {
  implicit def fromEntityUnmarshallerUser: FromEntityUnmarshaller[User]



  implicit def toEntityMarshallerUserCompletearray: ToEntityMarshaller[Seq[UserComplete]]

}

