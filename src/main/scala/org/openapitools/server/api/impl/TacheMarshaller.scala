package org.openapitools.server.api.impl

import spray.json.DefaultJsonProtocol
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.openapitools.server.api.CategorieApiMarshaller
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import org.openapitools.server.model.TacheUpdate
import org.openapitools.server.model.Tache
import org.openapitools.server.api.TacheApiMarshaller
 import org.openapitools.server.model.TacheComplete


 // Define JSON formats for your case classes
trait JsonProtocolTache extends DefaultJsonProtocol {
  // Format for other case classes (Categorie, UserComplete, TacheComplete, etc.)
  implicit val tacheUpdateFormat: RootJsonFormat[TacheUpdate] = jsonFormat2(TacheUpdate)
  implicit val tacheCompleteFormat: RootJsonFormat[TacheComplete] = jsonFormat4(TacheComplete.apply)
  implicit val tacheFormat: RootJsonFormat[Tache] = jsonFormat2(Tache)
}

 
 object TacheMarshaller extends TacheApiMarshaller with SprayJsonSupport with JsonProtocolTache {

     override implicit def fromEntityUnmarshallerTacheUpdate: FromEntityUnmarshaller[TacheUpdate]= {
        sprayJsonUnmarshaller[TacheUpdate]
     }
     override implicit def fromEntityUnmarshallerTache: FromEntityUnmarshaller[Tache] = {
        sprayJsonUnmarshaller[Tache]
     }
     override implicit def toEntityMarshallerTacheComplete: ToEntityMarshaller[TacheComplete] = {
        sprayJsonMarshaller[TacheComplete]
    }

    override implicit def toEntityMarshallerTacheCompletearray: ToEntityMarshaller[Seq[TacheComplete]] = {
    sprayJsonMarshaller[Seq[TacheComplete]]
  }

 }


