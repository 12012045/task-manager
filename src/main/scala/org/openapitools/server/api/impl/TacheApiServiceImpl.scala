package org.openapitools.server.api.impl

import fr.persistance.Database
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.model.HttpResponse
import org.apache.pekko.http.scaladsl.model.headers.`Access-Control-Allow-Origin`
import org.apache.pekko.http.scaladsl.server.Directives.{complete, onComplete}
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.TacheApiService
import org.openapitools.server.model.{Tache, TacheComplete,TacheUpdate}
import scala.util.{Failure, Success}

object TacheApiServiceImpl extends TacheApiService{
        override def createTask(tache: Tache): Route = {
            onComplete(Database.createTask(tache)){
                case Success(_) => complete(200, "Operation successful")
                case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
            }
        }
        
        override def getTask(idTache: Int) (implicit toEntityMarshallerTacheComplete: ToEntityMarshaller[TacheComplete]): Route = {
            onComplete(Database.getTask(idTache)){
                case Success(data) => complete(data)
                case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
            } 
        }
        
        override def updateTask(tacheUpdate: TacheUpdate): Route =  {
            onComplete(Database.updateTask(tacheUpdate)){
                case Success(_) => complete(200, "Operation successful")
                case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
            } 
        }

        override def getTasks()(implicit toEntityMarshallerTacheCompletearray: ToEntityMarshaller[Seq[TacheComplete]]): Route = {
            onComplete(Database.geTasks){
                case Success(data) => complete(200,data)
                case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
            }
        }
}
