
package org.openapitools.server.api.impl

import fr.actors.DatabaseActor
import fr.persistance.Database
import org.apache.pekko.actor
import org.apache.pekko.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.server.Directives.{complete, onComplete, onSuccess}
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.UserApiService
import org.openapitools.server.model.{ User, UserComplete}

import scala.util.{Failure, Success}
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

class UserApiServiceImpl(databaseActor: ActorRef[DatabaseActor.Command])
                        (implicit val system: actor.typed.ActorSystem[_], val executionContext: ExecutionContext)
  extends UserApiService {

  // Define an implicit timeout for the ask pattern
  implicit val timeout: Timeout = 3.seconds

  override def createUser(user: User): Route = {
    onSuccess(databaseActor.ask(DatabaseActor.CreateUser(user, _))) {
      case DatabaseActor.UserCreated(userId) if userId > 0 =>
        complete((StatusCodes.Created, s"User created with ID: $userId"))
      case _ =>
        complete((StatusCodes.InternalServerError, "Failed to create user"))
    }
  }

    override def getUsers()(implicit toEntityMarshallerGetUsers200Response: ToEntityMarshaller[Seq[UserComplete]]): Route ={
        onComplete(Database.getUsers) {
            case Success(data) =>
                println("Returned users list")
                complete(200,data)
            case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
        }
    }
    
}
