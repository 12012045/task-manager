package org.openapitools.server.api.impl

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.openapitools.server.api.UserApiMarshaller
import org.openapitools.server.model.{User, UserComplete}
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import org.openapitools.server.api.UserApiMarshaller

// JsonProtocol implementation
trait JsonProtocolUsers extends DefaultJsonProtocol {
  // Format for other case classes (Categorie, UserComplete, TacheComplete, etc.)
  implicit val userFormat: RootJsonFormat[User] = jsonFormat3(User)
  implicit val userCompleteFormat: RootJsonFormat[UserComplete] = jsonFormat4(UserComplete.apply)
}

// UserMarshaller implementation
object UserMarshaller extends UserApiMarshaller with SprayJsonSupport with JsonProtocolUsers {

  override implicit def fromEntityUnmarshallerUser: FromEntityUnmarshaller[User] = {
    sprayJsonUnmarshaller[User]
  }

  override implicit def toEntityMarshallerUserCompletearray: ToEntityMarshaller[Seq[UserComplete]] = {
    sprayJsonMarshaller [Seq[UserComplete]]
  }
}

