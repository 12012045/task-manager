package org.openapitools.server.api.impl

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.openapitools.server.api.CategorieApiMarshaller
import org.openapitools.server.model.{Categorie, CategorieComplete, TacheComplete, UserComplete}
import spray.json.{DefaultJsonProtocol, RootJsonFormat}



// Your case classes (Categorie, UserComplete, TacheComplete, etc.) should be defined somewhere

// Define JSON formats for your case classes
trait JsonProtocol extends DefaultJsonProtocol {
  // Format for other case classes (Categorie, UserComplete, TacheComplete, etc.)
  implicit val userCompleteFormat: RootJsonFormat[UserComplete] = jsonFormat4(UserComplete)
  implicit val tacheCompleteFormat: RootJsonFormat[TacheComplete] = jsonFormat4(TacheComplete)
  // implicit val getCategories200ResponseFormat: RootJsonFormat[GetCategories200Response] = jsonFormat1(GetCategories200Response)
  implicit val categorieFormat: RootJsonFormat[Categorie] = jsonFormat2(Categorie)
  implicit val categorieCompleteFormat: RootJsonFormat[CategorieComplete] = jsonFormat3(CategorieComplete.apply)

}

// CategoryMarshaller implementation
object CategoryMarshaller extends CategorieApiMarshaller with SprayJsonSupport with JsonProtocol {

  override implicit def fromEntityUnmarshallerCategorie: FromEntityUnmarshaller[Categorie] = {
    sprayJsonUnmarshaller[Categorie]
  }

  override implicit def toEntityMarshallerCategorieComplete: ToEntityMarshaller[CategorieComplete] = {
    sprayJsonMarshaller[CategorieComplete]
  }

  override implicit def toEntityMarshallerCategorieCompletearray: ToEntityMarshaller[Seq[CategorieComplete]] = {
    sprayJsonMarshaller[Seq[CategorieComplete]]
  }

}

