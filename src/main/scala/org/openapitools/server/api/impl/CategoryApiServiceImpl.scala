
package org.openapitools.server.api.impl

import fr.persistance.Database
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.model.HttpResponse
import org.apache.pekko.http.scaladsl.model.headers.`Access-Control-Allow-Origin`
import org.apache.pekko.http.scaladsl.server.Directives.{complete, onComplete}
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.CategorieApiService
import org.openapitools.server.model.{Categorie, CategorieComplete}

import scala.util.{Failure, Success}
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal

object CategoryApiServiceImpl extends CategorieApiService {

  override def createCategory(categorie: Categorie): Route = {
    onComplete(Database.createCategory(categorie)) {
      case Success(_) => complete(200, "Operation successful")
      case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
    }
  }

  override def getCategories()(implicit toEntityMarshallerCategorieCompletearray: ToEntityMarshaller[Seq[CategorieComplete]]): Route = {
    onComplete(Database.getCategories) {
      case Success(data) =>
        println("Returned categories list")
        complete(200,data)
      case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
    }
  }

  override def getCategory(idCategorie: Int)(implicit toEntityMarshallerCategorieComplete: ToEntityMarshaller[CategorieComplete]): Route = {
    
    onComplete(Database.getCategory(idCategorie)){
      case Success (data) =>
        println("Category created")
        complete(200,data)
      case Failure(ex) => complete(500, s"An error occurred: ${ex.getMessage}")
    }
  }
}
