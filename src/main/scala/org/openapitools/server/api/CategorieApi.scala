package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.Categorie
import org.openapitools.server.model.CategorieComplete


class CategorieApi(
    categorieService: CategorieApiService,
    categorieMarshaller: CategorieApiMarshaller
) {

  
  import categorieMarshaller._

  lazy val route: Route =
    path("categorie") { 
      post {  
            entity(as[Categorie]){ categorie =>
              categorieService.createCategory(categorie = categorie)
            }
      }
    } ~
    path("categorie") { 
      get {  
            categorieService.getCategories()
      }
    } ~
    path("categorie" / IntNumber) { (idCategorie) => 
      get {  
            categorieService.getCategory(idCategorie = idCategorie)
      }
    }
}


trait CategorieApiService {

  def createCategory200: Route =
    complete((200, "opération effectuée avec succès"))
  def createCategory401: Route =
    complete((401, "cette catégorie existe déja"))
  /**
   * Code: 200, Message: opération effectuée avec succès
   * Code: 401, Message: cette catégorie existe déja
   */
  def createCategory(categorie: Categorie): Route

  def getCategories200(responseCategorieCompletearray: Seq[CategorieComplete])(implicit toEntityMarshallerCategorieCompletearray: ToEntityMarshaller[Seq[CategorieComplete]]): Route =
    complete((200, responseCategorieCompletearray))
  /**
   * Code: 200, Message: Opération effectuée avec succès, DataType: Seq[CategorieComplete]
   */
  def getCategories()
      (implicit toEntityMarshallerCategorieCompletearray: ToEntityMarshaller[Seq[CategorieComplete]]): Route

  def getCategory200(responseCategorieComplete: CategorieComplete)(implicit toEntityMarshallerCategorieComplete: ToEntityMarshaller[CategorieComplete]): Route =
    complete((200, responseCategorieComplete))
  def getCategory404: Route =
    complete((404, "aucune catégorie associé"))
  /**
   * Code: 200, Message: Opération effectuée avec succès, DataType: CategorieComplete
   * Code: 404, Message: aucune catégorie associé
   */
  def getCategory(idCategorie: Int)
      (implicit toEntityMarshallerCategorieComplete: ToEntityMarshaller[CategorieComplete]): Route

}

trait CategorieApiMarshaller {
  implicit def fromEntityUnmarshallerCategorie: FromEntityUnmarshaller[Categorie]



  implicit def toEntityMarshallerCategorieComplete: ToEntityMarshaller[CategorieComplete]

  implicit def toEntityMarshallerCategorieCompletearray: ToEntityMarshaller[Seq[CategorieComplete]]

}

