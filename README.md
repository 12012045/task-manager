# task-manager

## Description 
  Système permettant d'organiser et de gérer ses tâches
  ## API

  ### Categorie

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.CategorieController`|akka-http API controller|
          |`org.openapitools.server.api.CategorieApi`|Representing trait|
              |`org.openapitools.server.api.CategorieApiImpl`|Default implementation|

                * `POST /api/categorie` - Créer une nouvelle catégorie de tâches
                * `GET /api/categorie` - Accès aux catégories
                * `GET /api/categorie/{idCategorie}` - Accès à une catégorie

  ### Tache

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.TacheController`|akka-http API controller|
          |`org.openapitools.server.api.TacheApi`|Representing trait|
              |`org.openapitools.server.api.TacheApiImpl`|Default implementation|

                * `POST /api/tache` - Créer une nouvelle  tâches
                * `GET /api/tache/{idTache}` - Accès à une tâche
                * `PUT /api/tache` - Modifier une tâche

  ### User

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.UserController`|akka-http API controller|
          |`org.openapitools.server.api.UserApi`|Representing trait|
              |`org.openapitools.server.api.UserApiImpl`|Default implementation|

                * `POST /api/user` - Créer un nouvel utilsateur
                * `GET /api/user` - Récupérer la liste des utilisateurs

  ##  Exemples de commandes curl 
  ###  user
>
>  ##### creation d'un utilisateur
>     curl -X POST -H "Content-Type: application/json" -d '{
>              "username": "john_doe",
>              "postalCode": "92200"
>            }' http://localhost:8081/user
>        
>  #### recuperer les utilisateurs
>     curl -X GET http://localhost:8081/user  
  ### categorie
>   #### creation d'une categorie
>      curl -X POST -H "Content-Type: application/json" -d '{                                
>            "name": "courses", 
>            "idUser": 1      
>           }' http://localhost:8081/categorie
>
>   #### recuperer la liste des categories
>      curl -X GET http://localhost:8081/categorie
>
>   ### recuperer une categorie
>      curl -X GET http://localhost:8081/categorie/1
  ### task
>  #### creation d'une tache 
>     curl -X POST -H "Content-Type: application/json" -d '{                                
>            "idCategory": 1,
>            "label":"Faire les courses"
>          }' http://localhost:8081/tache
>  #### recuperer toutes les taches
>      curl -X GET http://localhost:8081/tache
>  #### recuperer une tache 
>         curl -X GET http://localhost:8081/tache/1 
>#### Modifier le statut d'une tache
>          curl -X PUT -H "Content-Type: application/json" -d '{                                 
>             "idTask": 1,              
>             "status":"encours"          
>           }' http://localhost:8081/tache

## Diagramme de séquence du traitement d'une requête de création d'utilisateur (requête post vers le endpoint "/user")
```mermaid
sequenceDiagram
autonumber
    Client->>+Server: post request to "/user" endpoint
    Server->>+DatabaseActor: createUser(user)
    DatabaseActor->>+RequestActor: getCityName(user)
    RequestActor->>+ExternalApi : getCityName(user.postalCode)
    ExternalApi->>+RequestActor : cityName
    RequestActor->>+RequestActor: updateUser(user,cityName)
    RequestActor->>+DatabaseActor: updateUser(updatedUser)
    DatabaseActor->>+DatabaseActor :saveUser(updatedUser)
```
## Croquis front-end

![alt croquis_frontend](./croquis_front_task_manager.png )

Nos documents (images, textes, vidéos) sont publiés sous licences Creative Commons BY-SA.

